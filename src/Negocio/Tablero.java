/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

/**
 *
 * @author rozo-
 */
public class Tablero {

    // private Ficha [][]myTablero;
    private int[][] myTablero;
    private int[][] myTableroP;
    int pos, posp = 0;

    //Adicione los atributos que considere necesarios para el correcto fucnioanmiento de su aplicación, si y solo si , no violen ninguna regla en POO
    public Tablero() {
    }

    /**
     * Constructor para iniciar el juego
     *
     * @param i_alfil Posición de la fila para el alfil
     * @param j_alfil Posición de la columna para el alfil
     * @param i_peon Posición de la fila para el peon
     * @param j_peon Posición de la columna para el alfil
     * @param dirPeon true si el peón se mueve de arriba hacia abajo, o false en
     * caso contrario
     */
    public Tablero(int i_alfil, int j_alfil, int i_peon, int j_peon, boolean dirPeon) {
        System.out.println("algo");
        myTablero = new int[14][2];
        myTableroP = new int[8][2];
        guardarBloqueo(i_alfil, j_alfil, 0);
        moverPeon(i_peon, j_peon, dirPeon);

    }

    public void moverPeon(int xp, int yp, boolean dirPeon) {

        if (validar(xp, yp)) {
            if (!estaBloquada(xp, yp)) {
                System.out.println("peon "+xp+" "+yp);
                guardarPeon(xp, yp);
                if (dirPeon) {
                    moverPeon(xp + 1, yp, dirPeon);
                } else {
                    moverPeon(xp - 1, yp, dirPeon);
                }

            }//fin bloqueo
            else {
                moverAlfil();
            }
        }//gano
        System.out.println("gano");
    }

    public boolean validar(int x, int y) {
        if (x >= 0 && x <= 7 && y >= 0 && y <= 7) {
            return true;
        }
        return false;
    }

    public void guardarBloqueo(int pxa, int pya, int i) {
        if (validar(pxa, pya)) {
            guardar(pxa, pya);
            System.out.println("guarde "+ pxa + " " + pya);

            if (i <= 10 && i >= 0) {
                guardarBloqueo(pxa - 1, pya - 1, i + 1);
                if (i != 0) {
                    return;
                }
            }

            if (i > 10 || i == 0) {
                guardarBloqueo(pxa - 1, pya + 1, i + 11);
                if (i != 0) {
                    return;
                }
            }

            if (i <= 0 && i > -10) {
                guardarBloqueo(pxa + 1, pya - 1, i - 1);
                if (i != 0) {
                    return;
                }
            }

            if (i < -10 || i == 0) {
                guardarBloqueo(pxa + 1, pya + 1, i - 11);
                if (i != 0) {
                    return;
                }
            }
        };
    }

    public boolean estaBloquada(int xp, int yp) {
        for (int i = 0; i < myTablero.length; i++) {
            if (xp == myTablero[i][0] && yp == myTablero[i][1]) {
                return true;
            }
        }
        return false;
    }

    public String jugar() {

        return null;
    }

    private void guardar(int pxa, int pya) {
        myTablero[pos][0] = pxa;
        myTablero[pos][1] = pya;
        pos++;
    }

    private void guardarPeon(int xp, int yp) {
        myTableroP[posp][0] = xp;
        myTableroP[posp][1] = yp;
        posp++;
    }

    private void moverAlfil() {
        System.out.println("borre");
        myTablero = new int[14][2];
        myTableroP = new int[8][2];
        posp = 0;
        pos = 0;
        guardarBloqueo(2, 0, 0);
        moverPeon(5, 4, false);
    }
}
